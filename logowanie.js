var express = require('express');
var ejs = require('ejs');
var bodyParser = require('body-parser');
var mysql = require('mysql');
var $ = require('jQuery');

var connect = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "artbook"
});

var app = express();
app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(express.static('./public'));
app.get('/', function(req, res, next){
    var obj = {};
    connect.query('SELECT * FROM `posty`', function(err, rows){
        if(err) {
            console.log(err);
            return;
        }
        for(var z in rows){
            for(var z1 in rows){
                if(z1>0)
                {
                    z1--;
                }
                for(var z2 in rows){
                    if(z2>1)
                    {
                        z2= z2 - 2;
                    }
                    var titles = {
                        title0: rows[z].tytul_post,
                        title1: rows[z1].tytul_post,
                        title2: rows[z2].tytul_post
                    }
                    var dates = {
                        date0: rows[z].data_post,
                        date1: rows[z1].data_post,
                        date2: rows[z2].data_post
                    }
                    var contents = {
                        content0: rows[z].tresc_post,
                        content1: rows[z1].tresc_post,
                        content2: rows[z2].tresc_post
                    }
                }
            }
        }
        res.render('./index', {titles, dates, contents});
    });
});
module.exports = app;
app.get('/admin', function(req, res){
    res.render('./admin');
});
app.post('/admin', function(req, res){
    connect.connect(function(err){
        connect.on('error', function(err) {
            console.log("mysql error",err);
        });
        var sqlSelect = 'SELECT imie, nazwisko FROM konta WHERE imie = "'+req.body.login+'" AND nazwisko = "'+req.body.password+'";';
        connect.query(sqlSelect, function (err, rows){
            connect.on('error', function(err) {
                console.log("query error",err);
            });
            for(var i in rows) {
                if(req.body.login === rows[i].imie && req.body.password === rows[i].nazwisko){
                    res.render('./cms');
                } 
            }
        });
    });
});
app.post('/cms', function(req, res){
    connect.connect(function(err){
        connect.on('error', function(err){
            console.log("mysql error",err);
        });     
        var sqlInsert = 'INSERT INTO `posty`(`id_posty`, `tytul_post`, `data_post`, `tresc_post`) VALUES (NULL,"'+req.body.title+'", "'+req.body.date+'", "'+req.body.content+'");';
        connect.query(sqlInsert, function(err, rows){
            connect.on('error', function(err){
                console.log("query error",err);
            });
            console.log("Dodano post");
            res.render('./cms');
        });
    });
});


app.listen(3000);
console.log('Port serwera: 3000, PAMIETAJ O XAMPPIE XD');
